using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindGameObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //MAS USADOS

        //Busca el objeto cuyo nombre corresponda a Tierra
        GameObject.Find("Tierra"); 

        //Busca todos los objetos que tengan la etiqueta de Planet
        //al poder ser varios devuelve un arreglo de objetos
        GameObject.FindGameObjectsWithTag("Planet"); 


        //Es una busqueda por etiqueta tambien, 
        //pero en lugar de devolvernos el arreglo nos devuelve solo el primer elemento encontrado
        GameObject.FindGameObjectWithTag("Planet"); 

        //FIND mas especifico

        // Devuelve un objeto llamado Tierra.
        // Este elemtno debe ser raiz, no debe tener padres en la jerarquia.
        GameObject.Find("/Tierra");

        // Devuelve el objeto llamado Tierra,
        // Este elemento debe ser hijo del elemento Espacio.
        // Espacio debe ser un elemento raiz.
        GameObject.Find("/Espacio/Planetas/Tierra");

        // Devuelve un objeto de nombre Tierra,
        // Tierra debe ser hijo de un objeto Planetas que a su vez es Hijo de espacio, el cual puede tener otros parentescos.
        GameObject.Find("Espacio/Planetas/Tierra");
        
        //ADICIONALES
        //Busca unicamente entre elementos activos y devuelve la primera coincidencia con la etiqueta seleccionada
        GameObject.FindWithTag("Planet"); 
        //Busca todos los objetos que contengan el tipo indicado, no se recomienda porque es muy lento
        GameObject.FindObjectOfType(typeof(SpriteRenderer)); 

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
