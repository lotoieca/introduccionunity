using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lifeCycle : MonoBehaviour
{
    private void Awake()
    {
        //Debug.Log("Mensaje Normal");
        //Debug.LogWarning("Mensaje de alerta (Warning)");
        //Debug.LogError("Mensaje de error");
        Debug.Log("Awake ejecutado");
    }
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start ejecutado");
    }

    private void FixedUpdate()
    {
        Debug.Log("FixedUpdate ejecutado");
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update ejecutado");
    }
}
