using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class traslation : MonoBehaviour
{
    public float speedTraslation;
    private GameObject origin;
    // Start is called before the first frame update
    void Start()
    {
        if(gameObject.tag == "Moon")
        {
            origin = gameObject.transform.parent.gameObject;
        }
        else
        {
            origin = GameObject.Find("Sun");
        }
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.RotateAround(origin.transform.position, Vector3.forward, speedTraslation * Time.deltaTime);   
    }
}
