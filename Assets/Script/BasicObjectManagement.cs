using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicObjectManagement : MonoBehaviour
{
    public GameObject planet;
    public Sprite imagenPlaneta;
    private GameObject priv;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump")) //SPACE
        {
            GameObject newPlanet = Instantiate(planet);

            newPlanet.transform.position = new Vector3(Random.Range(-7f, 7f), Random.Range(-4f, 4f), 0);
            newPlanet.transform.Rotate(new Vector3(0,0,Random.Range(0,360f)));

            float scaleStandar = Random.Range(0.05f, 0.25f);
            newPlanet.transform.localScale = new Vector3(scaleStandar,scaleStandar,scaleStandar);

            newPlanet.GetComponent<SpriteRenderer>().sprite = imagenPlaneta;
        }

        if (Input.GetButtonDown("Fire1")) //CTRL LEFT OR CLICK
        {
            destroyItem();
        }
    }

    public void destroyItem()
    {
        GameObject[] planets = GameObject.FindGameObjectsWithTag("Planet");
        if(planets.Length > 1)
        {
            Destroy(planets[planets.Length-1]);
        }
    } 
}
